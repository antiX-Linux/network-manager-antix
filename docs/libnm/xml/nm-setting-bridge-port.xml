<?xml version="1.0"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
               "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
  <!ENTITY version SYSTEM "version.xml">
]>

<refentry id="NMSettingBridgePort">
<refmeta>
<refentrytitle role="top_of_page" id="NMSettingBridgePort.top_of_page">NMSettingBridgePort</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>LIBNM Library</refmiscinfo>
</refmeta>
<refnamediv>
<refname>NMSettingBridgePort</refname>
<refpurpose>Describes connection properties for bridge ports</refpurpose>
</refnamediv>

<refsect1 id="NMSettingBridgePort.functions" role="functions_proto">
<title role="functions_proto.title">Functions</title>
<informaltable pgwide="1" frame="none">
<tgroup cols="2">
<colspec colname="functions_return" colwidth="150px"/>
<colspec colname="functions_name"/>
<tbody>
<row><entry role="function_type"><link linkend="NMSetting"><returnvalue>NMSetting</returnvalue></link>&#160;*
</entry><entry role="function_name"><link linkend="nm-setting-bridge-port-new">nm_setting_bridge_port_new</link>&#160;<phrase role="c_punctuation">()</phrase></entry></row>
<row><entry role="function_type"><link linkend="guint16"><returnvalue>guint16</returnvalue></link>
</entry><entry role="function_name"><link linkend="nm-setting-bridge-port-get-priority">nm_setting_bridge_port_get_priority</link>&#160;<phrase role="c_punctuation">()</phrase></entry></row>
<row><entry role="function_type"><link linkend="guint16"><returnvalue>guint16</returnvalue></link>
</entry><entry role="function_name"><link linkend="nm-setting-bridge-port-get-path-cost">nm_setting_bridge_port_get_path_cost</link>&#160;<phrase role="c_punctuation">()</phrase></entry></row>
<row><entry role="function_type"><link linkend="gboolean"><returnvalue>gboolean</returnvalue></link>
</entry><entry role="function_name"><link linkend="nm-setting-bridge-port-get-hairpin-mode">nm_setting_bridge_port_get_hairpin_mode</link>&#160;<phrase role="c_punctuation">()</phrase></entry></row>

</tbody>
</tgroup>
</informaltable>
</refsect1>
<refsect1 id="NMSettingBridgePort.properties" role="properties">
<title role="properties.title">Properties</title>
<informaltable frame="none">
<tgroup cols="3">
<colspec colname="properties_type" colwidth="150px"/>
<colspec colname="properties_name" colwidth="300px"/>
<colspec colname="properties_flags" colwidth="200px"/>
<tbody>
<row><entry role="property_type"><link linkend="gboolean"><type>gboolean</type></link></entry><entry role="property_name"><link linkend="NMSettingBridgePort--hairpin-mode">hairpin-mode</link></entry><entry role="property_flags">Read / Write</entry></row>
<row><entry role="property_type"><link linkend="guint"><type>guint</type></link></entry><entry role="property_name"><link linkend="NMSettingBridgePort--path-cost">path-cost</link></entry><entry role="property_flags">Read / Write / Construct</entry></row>
<row><entry role="property_type"><link linkend="guint"><type>guint</type></link></entry><entry role="property_name"><link linkend="NMSettingBridgePort--priority">priority</link></entry><entry role="property_flags">Read / Write / Construct</entry></row>

</tbody>
</tgroup>
</informaltable>
</refsect1>
<refsect1 id="NMSettingBridgePort.other" role="other_proto">
<title role="other_proto.title">Types and Values</title>
<informaltable role="enum_members_table" pgwide="1" frame="none">
<tgroup cols="2">
<colspec colname="name" colwidth="150px"/>
<colspec colname="description"/>
<tbody>
<row><entry role="define_keyword">#define</entry><entry role="function_name"><link linkend="NM-SETTING-BRIDGE-PORT-SETTING-NAME:CAPS">NM_SETTING_BRIDGE_PORT_SETTING_NAME</link></entry></row>
<row><entry role="define_keyword">#define</entry><entry role="function_name"><link linkend="NM-SETTING-BRIDGE-PORT-PRIORITY:CAPS">NM_SETTING_BRIDGE_PORT_PRIORITY</link></entry></row>
<row><entry role="define_keyword">#define</entry><entry role="function_name"><link linkend="NM-SETTING-BRIDGE-PORT-PATH-COST:CAPS">NM_SETTING_BRIDGE_PORT_PATH_COST</link></entry></row>
<row><entry role="define_keyword">#define</entry><entry role="function_name"><link linkend="NM-SETTING-BRIDGE-PORT-HAIRPIN-MODE:CAPS">NM_SETTING_BRIDGE_PORT_HAIRPIN_MODE</link></entry></row>
<row><entry role="datatype_keyword"></entry><entry role="function_name"><link linkend="NMSettingBridgePort-struct">NMSettingBridgePort</link></entry></row>

</tbody>
</tgroup>
</informaltable>
</refsect1>
<refsect1 id="NMSettingBridgePort.object-hierarchy" role="object_hierarchy">
<title role="object_hierarchy.title">Object Hierarchy</title>
<screen>    <link linkend="GObject">GObject</link>
    <phrase role="lineart">&#9584;&#9472;&#9472;</phrase> <link linkend="NMSetting">NMSetting</link>
        <phrase role="lineart">&#9584;&#9472;&#9472;</phrase> NMSettingBridgePort
</screen>
</refsect1>


<refsect1 id="NMSettingBridgePort.description" role="desc">
<title role="desc.title">Description</title>
<para>The <link linkend="NMSettingBridgePort"><type>NMSettingBridgePort</type></link> object is a <link linkend="NMSetting"><type>NMSetting</type></link> subclass that describes
optional properties that apply to bridge ports.</para>

</refsect1>
<refsect1 id="NMSettingBridgePort.functions_details" role="details">
<title role="details.title">Functions</title>
<refsect2 id="nm-setting-bridge-port-new" role="function">
<title>nm_setting_bridge_port_new&#160;()</title>
<indexterm zone="nm-setting-bridge-port-new"><primary>nm_setting_bridge_port_new</primary></indexterm>
<programlisting language="C"><link linkend="NMSetting"><returnvalue>NMSetting</returnvalue></link>&#160;*
nm_setting_bridge_port_new (<parameter><type>void</type></parameter>);</programlisting>
<para>Creates a new <link linkend="NMSettingBridgePort"><type>NMSettingBridgePort</type></link> object with default values.</para>
<refsect3 id="nm-setting-bridge-port-new.returns" role="returns">
<title>Returns</title>
<para> the new empty <link linkend="NMSettingBridgePort"><type>NMSettingBridgePort</type></link> object. </para>
<para><emphasis role="annotation">[<acronym>transfer full</acronym>]</emphasis></para>
</refsect3></refsect2>
<refsect2 id="nm-setting-bridge-port-get-priority" role="function">
<title>nm_setting_bridge_port_get_priority&#160;()</title>
<indexterm zone="nm-setting-bridge-port-get-priority"><primary>nm_setting_bridge_port_get_priority</primary></indexterm>
<programlisting language="C"><link linkend="guint16"><returnvalue>guint16</returnvalue></link>
nm_setting_bridge_port_get_priority (<parameter><link linkend="NMSettingBridgePort"><type>NMSettingBridgePort</type></link> *setting</parameter>);</programlisting>
<refsect3 id="nm-setting-bridge-port-get-priority.parameters" role="parameters">
<title>Parameters</title>
<informaltable role="parameters_table" pgwide="1" frame="none">
<tgroup cols="3">
<colspec colname="parameters_name" colwidth="150px"/>
<colspec colname="parameters_description"/>
<colspec colname="parameters_annotations" colwidth="200px"/>
<tbody>
<row><entry role="parameter_name"><para>setting</para></entry>
<entry role="parameter_description"><para>the <link linkend="NMSettingBridgePort"><type>NMSettingBridgePort</type></link></para></entry>
<entry role="parameter_annotations"></entry></row>
</tbody></tgroup></informaltable>
</refsect3><refsect3 id="nm-setting-bridge-port-get-priority.returns" role="returns">
<title>Returns</title>
<para> the <link linkend="NMSettingBridgePort--priority"><type>“priority”</type></link> property of the setting</para>
</refsect3></refsect2>
<refsect2 id="nm-setting-bridge-port-get-path-cost" role="function">
<title>nm_setting_bridge_port_get_path_cost&#160;()</title>
<indexterm zone="nm-setting-bridge-port-get-path-cost"><primary>nm_setting_bridge_port_get_path_cost</primary></indexterm>
<programlisting language="C"><link linkend="guint16"><returnvalue>guint16</returnvalue></link>
nm_setting_bridge_port_get_path_cost (<parameter><link linkend="NMSettingBridgePort"><type>NMSettingBridgePort</type></link> *setting</parameter>);</programlisting>
<refsect3 id="nm-setting-bridge-port-get-path-cost.parameters" role="parameters">
<title>Parameters</title>
<informaltable role="parameters_table" pgwide="1" frame="none">
<tgroup cols="3">
<colspec colname="parameters_name" colwidth="150px"/>
<colspec colname="parameters_description"/>
<colspec colname="parameters_annotations" colwidth="200px"/>
<tbody>
<row><entry role="parameter_name"><para>setting</para></entry>
<entry role="parameter_description"><para>the <link linkend="NMSettingBridgePort"><type>NMSettingBridgePort</type></link></para></entry>
<entry role="parameter_annotations"></entry></row>
</tbody></tgroup></informaltable>
</refsect3><refsect3 id="nm-setting-bridge-port-get-path-cost.returns" role="returns">
<title>Returns</title>
<para> the <link linkend="NMSettingBridgePort--path-cost"><type>“path-cost”</type></link> property of the setting</para>
</refsect3></refsect2>
<refsect2 id="nm-setting-bridge-port-get-hairpin-mode" role="function">
<title>nm_setting_bridge_port_get_hairpin_mode&#160;()</title>
<indexterm zone="nm-setting-bridge-port-get-hairpin-mode"><primary>nm_setting_bridge_port_get_hairpin_mode</primary></indexterm>
<programlisting language="C"><link linkend="gboolean"><returnvalue>gboolean</returnvalue></link>
nm_setting_bridge_port_get_hairpin_mode
                               (<parameter><link linkend="NMSettingBridgePort"><type>NMSettingBridgePort</type></link> *setting</parameter>);</programlisting>
<refsect3 id="nm-setting-bridge-port-get-hairpin-mode.parameters" role="parameters">
<title>Parameters</title>
<informaltable role="parameters_table" pgwide="1" frame="none">
<tgroup cols="3">
<colspec colname="parameters_name" colwidth="150px"/>
<colspec colname="parameters_description"/>
<colspec colname="parameters_annotations" colwidth="200px"/>
<tbody>
<row><entry role="parameter_name"><para>setting</para></entry>
<entry role="parameter_description"><para>the <link linkend="NMSettingBridgePort"><type>NMSettingBridgePort</type></link></para></entry>
<entry role="parameter_annotations"></entry></row>
</tbody></tgroup></informaltable>
</refsect3><refsect3 id="nm-setting-bridge-port-get-hairpin-mode.returns" role="returns">
<title>Returns</title>
<para> the <link linkend="NMSettingBridgePort--hairpin-mode"><type>“hairpin-mode”</type></link> property of the setting</para>
</refsect3></refsect2>

</refsect1>
<refsect1 id="NMSettingBridgePort.other_details" role="details">
<title role="details.title">Types and Values</title>
<refsect2 id="NM-SETTING-BRIDGE-PORT-SETTING-NAME:CAPS" role="macro">
<title>NM_SETTING_BRIDGE_PORT_SETTING_NAME</title>
<indexterm zone="NM-SETTING-BRIDGE-PORT-SETTING-NAME:CAPS"><primary>NM_SETTING_BRIDGE_PORT_SETTING_NAME</primary></indexterm>
<programlisting language="C">#define NM_SETTING_BRIDGE_PORT_SETTING_NAME "bridge-port"
</programlisting>
</refsect2>
<refsect2 id="NM-SETTING-BRIDGE-PORT-PRIORITY:CAPS" role="macro">
<title>NM_SETTING_BRIDGE_PORT_PRIORITY</title>
<indexterm zone="NM-SETTING-BRIDGE-PORT-PRIORITY:CAPS"><primary>NM_SETTING_BRIDGE_PORT_PRIORITY</primary></indexterm>
<programlisting language="C">#define NM_SETTING_BRIDGE_PORT_PRIORITY     "priority"
</programlisting>
</refsect2>
<refsect2 id="NM-SETTING-BRIDGE-PORT-PATH-COST:CAPS" role="macro">
<title>NM_SETTING_BRIDGE_PORT_PATH_COST</title>
<indexterm zone="NM-SETTING-BRIDGE-PORT-PATH-COST:CAPS"><primary>NM_SETTING_BRIDGE_PORT_PATH_COST</primary></indexterm>
<programlisting language="C">#define NM_SETTING_BRIDGE_PORT_PATH_COST    "path-cost"
</programlisting>
</refsect2>
<refsect2 id="NM-SETTING-BRIDGE-PORT-HAIRPIN-MODE:CAPS" role="macro">
<title>NM_SETTING_BRIDGE_PORT_HAIRPIN_MODE</title>
<indexterm zone="NM-SETTING-BRIDGE-PORT-HAIRPIN-MODE:CAPS"><primary>NM_SETTING_BRIDGE_PORT_HAIRPIN_MODE</primary></indexterm>
<programlisting language="C">#define NM_SETTING_BRIDGE_PORT_HAIRPIN_MODE "hairpin-mode"
</programlisting>
</refsect2>
<refsect2 id="NMSettingBridgePort-struct" role="struct">
<title>NMSettingBridgePort</title>
<indexterm zone="NMSettingBridgePort-struct"><primary>NMSettingBridgePort</primary></indexterm>
<programlisting language="C">typedef struct _NMSettingBridgePort NMSettingBridgePort;</programlisting>
</refsect2>

</refsect1>
<refsect1 id="NMSettingBridgePort.property-details" role="property_details">
<title role="property_details.title">Property Details</title>
<refsect2 id="NMSettingBridgePort--hairpin-mode" role="property"><title>The <literal>“hairpin-mode”</literal> property</title>
<indexterm zone="NMSettingBridgePort--hairpin-mode"><primary>NMSettingBridgePort:hairpin-mode</primary></indexterm>
<programlisting>  “hairpin-mode”             <link linkend="gboolean"><type>gboolean</type></link></programlisting>
<para>Enables or disabled "hairpin mode" for the port, which allows frames to
be sent back out through the port the frame was received on.</para>
<para>Flags: Read / Write</para>
<para>Default value: FALSE</para>
</refsect2>
<refsect2 id="NMSettingBridgePort--path-cost" role="property"><title>The <literal>“path-cost”</literal> property</title>
<indexterm zone="NMSettingBridgePort--path-cost"><primary>NMSettingBridgePort:path-cost</primary></indexterm>
<programlisting>  “path-cost”                <link linkend="guint"><type>guint</type></link></programlisting>
<para>The Spanning Tree Protocol (STP) port cost for destinations via this
port.</para>
<para>Flags: Read / Write / Construct</para>
<para>Allowed values: &lt;= 65535</para>
<para>Default value: 100</para>
</refsect2>
<refsect2 id="NMSettingBridgePort--priority" role="property"><title>The <literal>“priority”</literal> property</title>
<indexterm zone="NMSettingBridgePort--priority"><primary>NMSettingBridgePort:priority</primary></indexterm>
<programlisting>  “priority”                 <link linkend="guint"><type>guint</type></link></programlisting>
<para>The Spanning Tree Protocol (STP) priority of this bridge port.</para>
<para>Flags: Read / Write / Construct</para>
<para>Allowed values: &lt;= 63</para>
<para>Default value: 32</para>
</refsect2>

</refsect1>

</refentry>
