<?xml version="1.0"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
               "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
  <!ENTITY version SYSTEM "version.xml">
]>

<refentry id="NMSettingBluetooth">
<refmeta>
<refentrytitle role="top_of_page" id="NMSettingBluetooth.top_of_page">NMSettingBluetooth</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>LIBNM Library</refmiscinfo>
</refmeta>
<refnamediv>
<refname>NMSettingBluetooth</refname>
<refpurpose>Describes Bluetooth connection properties</refpurpose>
</refnamediv>

<refsect1 id="NMSettingBluetooth.functions" role="functions_proto">
<title role="functions_proto.title">Functions</title>
<informaltable pgwide="1" frame="none">
<tgroup cols="2">
<colspec colname="functions_return" colwidth="150px"/>
<colspec colname="functions_name"/>
<tbody>
<row><entry role="function_type"><link linkend="NMSetting"><returnvalue>NMSetting</returnvalue></link>&#160;*
</entry><entry role="function_name"><link linkend="nm-setting-bluetooth-new">nm_setting_bluetooth_new</link>&#160;<phrase role="c_punctuation">()</phrase></entry></row>
<row><entry role="function_type">const <link linkend="char"><returnvalue>char</returnvalue></link>&#160;*
</entry><entry role="function_name"><link linkend="nm-setting-bluetooth-get-bdaddr">nm_setting_bluetooth_get_bdaddr</link>&#160;<phrase role="c_punctuation">()</phrase></entry></row>
<row><entry role="function_type">const <link linkend="char"><returnvalue>char</returnvalue></link>&#160;*
</entry><entry role="function_name"><link linkend="nm-setting-bluetooth-get-connection-type">nm_setting_bluetooth_get_connection_type</link>&#160;<phrase role="c_punctuation">()</phrase></entry></row>

</tbody>
</tgroup>
</informaltable>
</refsect1>
<refsect1 id="NMSettingBluetooth.properties" role="properties">
<title role="properties.title">Properties</title>
<informaltable frame="none">
<tgroup cols="3">
<colspec colname="properties_type" colwidth="150px"/>
<colspec colname="properties_name" colwidth="300px"/>
<colspec colname="properties_flags" colwidth="200px"/>
<tbody>
<row><entry role="property_type"><link linkend="gchar"><type>gchar</type></link>&#160;*</entry><entry role="property_name"><link linkend="NMSettingBluetooth--bdaddr">bdaddr</link></entry><entry role="property_flags">Read / Write</entry></row>
<row><entry role="property_type"><link linkend="gchar"><type>gchar</type></link>&#160;*</entry><entry role="property_name"><link linkend="NMSettingBluetooth--type">type</link></entry><entry role="property_flags">Read / Write</entry></row>

</tbody>
</tgroup>
</informaltable>
</refsect1>
<refsect1 id="NMSettingBluetooth.other" role="other_proto">
<title role="other_proto.title">Types and Values</title>
<informaltable role="enum_members_table" pgwide="1" frame="none">
<tgroup cols="2">
<colspec colname="name" colwidth="150px"/>
<colspec colname="description"/>
<tbody>
<row><entry role="define_keyword">#define</entry><entry role="function_name"><link linkend="NM-SETTING-BLUETOOTH-SETTING-NAME:CAPS">NM_SETTING_BLUETOOTH_SETTING_NAME</link></entry></row>
<row><entry role="define_keyword">#define</entry><entry role="function_name"><link linkend="NM-SETTING-BLUETOOTH-BDADDR:CAPS">NM_SETTING_BLUETOOTH_BDADDR</link></entry></row>
<row><entry role="define_keyword">#define</entry><entry role="function_name"><link linkend="NM-SETTING-BLUETOOTH-TYPE:CAPS">NM_SETTING_BLUETOOTH_TYPE</link></entry></row>
<row><entry role="define_keyword">#define</entry><entry role="function_name"><link linkend="NM-SETTING-BLUETOOTH-TYPE-DUN:CAPS">NM_SETTING_BLUETOOTH_TYPE_DUN</link></entry></row>
<row><entry role="define_keyword">#define</entry><entry role="function_name"><link linkend="NM-SETTING-BLUETOOTH-TYPE-PANU:CAPS">NM_SETTING_BLUETOOTH_TYPE_PANU</link></entry></row>
<row><entry role="datatype_keyword"></entry><entry role="function_name"><link linkend="NMSettingBluetooth-struct">NMSettingBluetooth</link></entry></row>

</tbody>
</tgroup>
</informaltable>
</refsect1>
<refsect1 id="NMSettingBluetooth.object-hierarchy" role="object_hierarchy">
<title role="object_hierarchy.title">Object Hierarchy</title>
<screen>    <link linkend="GObject">GObject</link>
    <phrase role="lineart">&#9584;&#9472;&#9472;</phrase> <link linkend="NMSetting">NMSetting</link>
        <phrase role="lineart">&#9584;&#9472;&#9472;</phrase> NMSettingBluetooth
</screen>
</refsect1>


<refsect1 id="NMSettingBluetooth.description" role="desc">
<title role="desc.title">Description</title>
<para>The <link linkend="NMSettingBluetooth"><type>NMSettingBluetooth</type></link> object is a <link linkend="NMSetting"><type>NMSetting</type></link> subclass that describes
properties necessary for connection to devices that provide network
connections via the Bluetooth Dial-Up Networking (DUN) and Network Access
Point (NAP) profiles.</para>

</refsect1>
<refsect1 id="NMSettingBluetooth.functions_details" role="details">
<title role="details.title">Functions</title>
<refsect2 id="nm-setting-bluetooth-new" role="function">
<title>nm_setting_bluetooth_new&#160;()</title>
<indexterm zone="nm-setting-bluetooth-new"><primary>nm_setting_bluetooth_new</primary></indexterm>
<programlisting language="C"><link linkend="NMSetting"><returnvalue>NMSetting</returnvalue></link>&#160;*
nm_setting_bluetooth_new (<parameter><type>void</type></parameter>);</programlisting>
<para>Creates a new <link linkend="NMSettingBluetooth"><type>NMSettingBluetooth</type></link> object with default values.</para>
<refsect3 id="nm-setting-bluetooth-new.returns" role="returns">
<title>Returns</title>
<para> the new empty <link linkend="NMSettingBluetooth"><type>NMSettingBluetooth</type></link> object. </para>
<para><emphasis role="annotation">[<acronym>transfer full</acronym>]</emphasis></para>
</refsect3></refsect2>
<refsect2 id="nm-setting-bluetooth-get-bdaddr" role="function">
<title>nm_setting_bluetooth_get_bdaddr&#160;()</title>
<indexterm zone="nm-setting-bluetooth-get-bdaddr"><primary>nm_setting_bluetooth_get_bdaddr</primary></indexterm>
<programlisting language="C">const <link linkend="char"><returnvalue>char</returnvalue></link>&#160;*
nm_setting_bluetooth_get_bdaddr (<parameter><link linkend="NMSettingBluetooth"><type>NMSettingBluetooth</type></link> *setting</parameter>);</programlisting>
<para>Gets the Bluetooth address of the remote device which this setting
describes a connection to.</para>
<refsect3 id="nm-setting-bluetooth-get-bdaddr.parameters" role="parameters">
<title>Parameters</title>
<informaltable role="parameters_table" pgwide="1" frame="none">
<tgroup cols="3">
<colspec colname="parameters_name" colwidth="150px"/>
<colspec colname="parameters_description"/>
<colspec colname="parameters_annotations" colwidth="200px"/>
<tbody>
<row><entry role="parameter_name"><para>setting</para></entry>
<entry role="parameter_description"><para>the <link linkend="NMSettingBluetooth"><type>NMSettingBluetooth</type></link></para></entry>
<entry role="parameter_annotations"></entry></row>
</tbody></tgroup></informaltable>
</refsect3><refsect3 id="nm-setting-bluetooth-get-bdaddr.returns" role="returns">
<title>Returns</title>
<para> the Bluetooth address</para>
</refsect3></refsect2>
<refsect2 id="nm-setting-bluetooth-get-connection-type" role="function">
<title>nm_setting_bluetooth_get_connection_type&#160;()</title>
<indexterm zone="nm-setting-bluetooth-get-connection-type"><primary>nm_setting_bluetooth_get_connection_type</primary></indexterm>
<programlisting language="C">const <link linkend="char"><returnvalue>char</returnvalue></link>&#160;*
nm_setting_bluetooth_get_connection_type
                               (<parameter><link linkend="NMSettingBluetooth"><type>NMSettingBluetooth</type></link> *setting</parameter>);</programlisting>
<para>Returns the connection method for communicating with the remote device (i.e.
either DUN to a DUN-capable device or PANU to a NAP-capable device).</para>
<refsect3 id="nm-setting-bluetooth-get-connection-type.parameters" role="parameters">
<title>Parameters</title>
<informaltable role="parameters_table" pgwide="1" frame="none">
<tgroup cols="3">
<colspec colname="parameters_name" colwidth="150px"/>
<colspec colname="parameters_description"/>
<colspec colname="parameters_annotations" colwidth="200px"/>
<tbody>
<row><entry role="parameter_name"><para>setting</para></entry>
<entry role="parameter_description"><para>the <link linkend="NMSettingBluetooth"><type>NMSettingBluetooth</type></link></para></entry>
<entry role="parameter_annotations"></entry></row>
</tbody></tgroup></informaltable>
</refsect3><refsect3 id="nm-setting-bluetooth-get-connection-type.returns" role="returns">
<title>Returns</title>
<para> the type, either <link linkend="NM-SETTING-BLUETOOTH-TYPE-PANU:CAPS"><literal>NM_SETTING_BLUETOOTH_TYPE_PANU</literal></link> or
<link linkend="NM-SETTING-BLUETOOTH-TYPE-DUN:CAPS"><literal>NM_SETTING_BLUETOOTH_TYPE_DUN</literal></link></para>
</refsect3></refsect2>

</refsect1>
<refsect1 id="NMSettingBluetooth.other_details" role="details">
<title role="details.title">Types and Values</title>
<refsect2 id="NM-SETTING-BLUETOOTH-SETTING-NAME:CAPS" role="macro">
<title>NM_SETTING_BLUETOOTH_SETTING_NAME</title>
<indexterm zone="NM-SETTING-BLUETOOTH-SETTING-NAME:CAPS"><primary>NM_SETTING_BLUETOOTH_SETTING_NAME</primary></indexterm>
<programlisting language="C">#define NM_SETTING_BLUETOOTH_SETTING_NAME "bluetooth"
</programlisting>
</refsect2>
<refsect2 id="NM-SETTING-BLUETOOTH-BDADDR:CAPS" role="macro">
<title>NM_SETTING_BLUETOOTH_BDADDR</title>
<indexterm zone="NM-SETTING-BLUETOOTH-BDADDR:CAPS"><primary>NM_SETTING_BLUETOOTH_BDADDR</primary></indexterm>
<programlisting language="C">#define NM_SETTING_BLUETOOTH_BDADDR    "bdaddr"
</programlisting>
</refsect2>
<refsect2 id="NM-SETTING-BLUETOOTH-TYPE:CAPS" role="macro">
<title>NM_SETTING_BLUETOOTH_TYPE</title>
<indexterm zone="NM-SETTING-BLUETOOTH-TYPE:CAPS"><primary>NM_SETTING_BLUETOOTH_TYPE</primary></indexterm>
<programlisting language="C">#define NM_SETTING_BLUETOOTH_TYPE      "type"
</programlisting>
</refsect2>
<refsect2 id="NM-SETTING-BLUETOOTH-TYPE-DUN:CAPS" role="macro">
<title>NM_SETTING_BLUETOOTH_TYPE_DUN</title>
<indexterm zone="NM-SETTING-BLUETOOTH-TYPE-DUN:CAPS"><primary>NM_SETTING_BLUETOOTH_TYPE_DUN</primary></indexterm>
<programlisting language="C">#define NM_SETTING_BLUETOOTH_TYPE_DUN  "dun"
</programlisting>
<para>Connection type describing a connection to devices that support the Bluetooth
DUN profile.</para>
</refsect2>
<refsect2 id="NM-SETTING-BLUETOOTH-TYPE-PANU:CAPS" role="macro">
<title>NM_SETTING_BLUETOOTH_TYPE_PANU</title>
<indexterm zone="NM-SETTING-BLUETOOTH-TYPE-PANU:CAPS"><primary>NM_SETTING_BLUETOOTH_TYPE_PANU</primary></indexterm>
<programlisting language="C">#define NM_SETTING_BLUETOOTH_TYPE_PANU "panu"
</programlisting>
<para>Connection type describing a connection to devices that support the Bluetooth
NAP (Network Access Point) protocol, which accepts connections via PANU.</para>
</refsect2>
<refsect2 id="NMSettingBluetooth-struct" role="struct">
<title>NMSettingBluetooth</title>
<indexterm zone="NMSettingBluetooth-struct"><primary>NMSettingBluetooth</primary></indexterm>
<programlisting language="C">typedef struct _NMSettingBluetooth NMSettingBluetooth;</programlisting>
</refsect2>

</refsect1>
<refsect1 id="NMSettingBluetooth.property-details" role="property_details">
<title role="property_details.title">Property Details</title>
<refsect2 id="NMSettingBluetooth--bdaddr" role="property"><title>The <literal>“bdaddr”</literal> property</title>
<indexterm zone="NMSettingBluetooth--bdaddr"><primary>NMSettingBluetooth:bdaddr</primary></indexterm>
<programlisting>  “bdaddr”                   <link linkend="gchar"><type>gchar</type></link>&#160;*</programlisting>
<para>The Bluetooth address of the device.</para>
<para>Flags: Read / Write</para>
<para>Default value: NULL</para>
</refsect2>
<refsect2 id="NMSettingBluetooth--type" role="property"><title>The <literal>“type”</literal> property</title>
<indexterm zone="NMSettingBluetooth--type"><primary>NMSettingBluetooth:type</primary></indexterm>
<programlisting>  “type”                     <link linkend="gchar"><type>gchar</type></link>&#160;*</programlisting>
<para>Either "dun" for Dial-Up Networking connections or "panu" for Personal
Area Networking connections to devices supporting the NAP profile.</para>
<para>Flags: Read / Write</para>
<para>Default value: NULL</para>
</refsect2>

</refsect1>

</refentry>
