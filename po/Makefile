# Makefile for program source directory in GNU NLS utilities package.
# Copyright (C) 1995, 1996, 1997 by Ulrich Drepper <drepper@gnu.ai.mit.edu>
# Copyright (C) 2004-2008 Rodney Dawes <dobey.pwns@gmail.com>
#
# This file may be copied and used freely without restrictions.  It may
# be used in projects which are not available under a GNU Public License,
# but which still want to provide support for the GNU gettext functionality.
#
# - Modified by Owen Taylor <otaylor@redhat.com> to use GETTEXT_PACKAGE
#   instead of PACKAGE and to look for po2tbl in ./ not in intl/
#
# - Modified by jacob berkman <jacob@ximian.com> to install
#   Makefile.in.in and po2tbl.sed.in for use with glib-gettextize
#
# - Modified by Rodney Dawes <dobey.pwns@gmail.com> for use with intltool
#
# We have the following line for use by intltoolize:
# INTLTOOL_MAKEFILE

GETTEXT_PACKAGE = NetworkManager
PACKAGE = NetworkManager
VERSION = 1.6.2

SHELL = /bin/bash

srcdir = .
top_srcdir = ..
top_builddir = ..


prefix = /usr
exec_prefix = ${prefix}
datadir = ${datarootdir}
datarootdir = ${prefix}/share
libdir = ${prefix}/lib/x86_64-linux-gnu
localedir = ${datarootdir}/locale
subdir = po
install_sh = ${SHELL} /home/anticap/src-debs/src-debs-antix/stretch/network-manager-1.6.2/build-aux/install-sh
# Automake >= 1.8 provides /bin/mkdir -p.
# Until it can be supposed, use the safe fallback:
mkdir_p = $(install_sh) -d

INSTALL = /usr/bin/install -c
INSTALL_DATA = ${INSTALL} -m 644

GMSGFMT = /usr/bin/msgfmt
MSGFMT = /usr/bin/msgfmt
XGETTEXT = /usr/bin/xgettext
INTLTOOL_UPDATE = /usr/bin/intltool-update
INTLTOOL_EXTRACT = /usr/bin/intltool-extract
MSGMERGE = INTLTOOL_EXTRACT="$(INTLTOOL_EXTRACT)" XGETTEXT="$(XGETTEXT)" srcdir=$(srcdir) $(INTLTOOL_UPDATE) --gettext-package $(GETTEXT_PACKAGE) --dist
GENPOT   = INTLTOOL_EXTRACT="$(INTLTOOL_EXTRACT)" XGETTEXT="$(XGETTEXT)" srcdir=$(srcdir) $(INTLTOOL_UPDATE) --gettext-package $(GETTEXT_PACKAGE) --pot

ALL_LINGUAS = 

PO_LINGUAS=$(shell if test -r $(srcdir)/LINGUAS; then grep -v "^\#" $(srcdir)/LINGUAS; else echo "$(ALL_LINGUAS)"; fi)

USER_LINGUAS=$(shell if test -n "$(LINGUAS)"; then LLINGUAS="$(LINGUAS)"; ALINGUAS="$(ALL_LINGUAS)"; for lang in $$LLINGUAS; do if test -n "`grep \^$$lang$$ $(srcdir)/LINGUAS 2>/dev/null`" -o -n "`echo $$ALINGUAS|tr ' ' '\n'|grep \^$$lang$$`"; then printf "$$lang "; fi; done; fi)

USE_LINGUAS=$(shell if test -n "$(USER_LINGUAS)" -o -n "$(LINGUAS)"; then LLINGUAS="$(USER_LINGUAS)"; else if test -n "$(PO_LINGUAS)"; then LLINGUAS="$(PO_LINGUAS)"; else LLINGUAS="$(ALL_LINGUAS)"; fi; fi; for lang in $$LLINGUAS; do printf "$$lang "; done)

POFILES=$(shell LINGUAS="$(PO_LINGUAS)"; for lang in $$LINGUAS; do printf "$$lang.po "; done)

DISTFILES = Makefile.in.in POTFILES.in $(POFILES)
EXTRA_DISTFILES = ChangeLog POTFILES.skip Makevars LINGUAS

POTFILES = \
	../clients/cli/agent.c \
	../clients/cli/common.c \
	../clients/cli/connections.c \
	../clients/cli/devices.c \
	../clients/cli/general.c \
	../clients/cli/nmcli.c \
	../clients/cli/polkit-agent.c \
	../clients/cli/settings.c \
	../clients/cli/utils.c \
	../clients/common/nm-polkit-listener.c \
	../clients/common/nm-secret-agent-simple.c \
	../clients/common/nm-vpn-helpers.c \
	../clients/nm-online.c \
	../clients/tui/newt/nmt-newt-utils.c \
	../clients/tui/nm-editor-utils.c \
	../clients/tui/nmt-connect-connection-list.c \
	../clients/tui/nmt-device-entry.c \
	../clients/tui/nmt-edit-connection-list.c \
	../clients/tui/nmt-editor-section.c \
	../clients/tui/nmt-editor.c \
	../clients/tui/nmt-mtu-entry.c \
	../clients/tui/nmt-page-bond.c \
	../clients/tui/nmt-page-bridge-port.c \
	../clients/tui/nmt-page-bridge.c \
	../clients/tui/nmt-page-dsl.c \
	../clients/tui/nmt-page-ethernet.c \
	../clients/tui/nmt-page-infiniband.c \
	../clients/tui/nmt-page-ip4.c \
	../clients/tui/nmt-page-ip6.c \
	../clients/tui/nmt-page-ip-tunnel.c \
	../clients/tui/nmt-page-ppp.c \
	../clients/tui/nmt-page-team-port.c \
	../clients/tui/nmt-page-team.c \
	../clients/tui/nmt-page-vlan.c \
	../clients/tui/nmt-page-wifi.c \
	../clients/tui/nmt-password-dialog.c \
	../clients/tui/nmt-password-fields.c \
	../clients/tui/nmt-route-editor.c \
	../clients/tui/nmt-route-table.c \
	../clients/tui/nmt-slave-list.c \
	../clients/tui/nmt-widget-list.c \
	../clients/tui/nmtui-connect.c \
	../clients/tui/nmtui-edit.c \
	../clients/tui/nmtui-hostname.c \
	../clients/tui/nmtui.c \
	../libnm-core/crypto.c \
	../libnm-core/crypto_gnutls.c \
	../libnm-core/crypto_nss.c \
	../libnm-core/nm-connection.c \
	../libnm-core/nm-dbus-utils.c \
	../libnm-core/nm-keyfile-reader.c \
	../libnm-core/nm-keyfile-writer.c \
	../libnm-core/nm-setting-8021x.c \
	../libnm-core/nm-setting-adsl.c \
	../libnm-core/nm-setting-bluetooth.c \
	../libnm-core/nm-setting-bond.c \
	../libnm-core/nm-setting-bridge-port.c \
	../libnm-core/nm-setting-bridge.c \
	../libnm-core/nm-setting-cdma.c \
	../libnm-core/nm-setting-connection.c \
	../libnm-core/nm-setting-dcb.c \
	../libnm-core/nm-setting-gsm.c \
	../libnm-core/nm-setting-infiniband.c \
	../libnm-core/nm-setting-ip-config.c \
	../libnm-core/nm-setting-ip4-config.c \
	../libnm-core/nm-setting-ip6-config.c \
	../libnm-core/nm-setting-ip-tunnel.c \
	../libnm-core/nm-setting-macsec.c \
	../libnm-core/nm-setting-macvlan.c \
	../libnm-core/nm-setting-olpc-mesh.c \
	../libnm-core/nm-setting-ppp.c \
	../libnm-core/nm-setting-pppoe.c \
	../libnm-core/nm-setting-proxy.c \
	../libnm-core/nm-setting-team.c \
	../libnm-core/nm-setting-team-port.c \
	../libnm-core/nm-setting-tun.c \
	../libnm-core/nm-setting-vlan.c \
	../libnm-core/nm-setting-vpn.c \
	../libnm-core/nm-setting-vxlan.c \
	../libnm-core/nm-setting-wimax.c \
	../libnm-core/nm-setting-wired.c \
	../libnm-core/nm-setting-wireless-security.c \
	../libnm-core/nm-setting-wireless.c \
	../libnm-core/nm-setting.c \
	../libnm-core/nm-utils.c \
	../libnm-core/nm-vpn-editor-plugin.c \
	../libnm-core/nm-vpn-plugin-info.c \
	../libnm-glib/nm-device.c \
	../libnm-glib/nm-remote-connection.c \
	../libnm-util/crypto.c \
	../libnm-util/crypto_gnutls.c \
	../libnm-util/crypto_nss.c \
	../libnm-util/nm-connection.c \
	../libnm-util/nm-setting.c \
	../libnm-util/nm-setting-8021x.c \
	../libnm-util/nm-setting-adsl.c \
	../libnm-util/nm-setting-bluetooth.c \
	../libnm-util/nm-setting-bond.c \
	../libnm-util/nm-setting-bridge-port.c \
	../libnm-util/nm-setting-bridge.c \
	../libnm-util/nm-setting-cdma.c \
	../libnm-util/nm-setting-connection.c \
	../libnm-util/nm-setting-dcb.c \
	../libnm-util/nm-setting-gsm.c \
	../libnm-util/nm-setting-infiniband.c \
	../libnm-util/nm-setting-ip4-config.c \
	../libnm-util/nm-setting-ip6-config.c \
	../libnm-util/nm-setting-olpc-mesh.c \
	../libnm-util/nm-setting-ppp.c \
	../libnm-util/nm-setting-pppoe.c \
	../libnm-util/nm-setting-team.c \
	../libnm-util/nm-setting-vlan.c \
	../libnm-util/nm-setting-vpn.c \
	../libnm-util/nm-setting-wimax.c \
	../libnm-util/nm-setting-wired.c \
	../libnm-util/nm-setting-wireless-security.c \
	../libnm-util/nm-setting-wireless.c \
	../libnm-util/nm-utils.c \
	../libnm/nm-device-adsl.c \
	../libnm/nm-device-bond.c \
	../libnm/nm-device-bridge.c \
	../libnm/nm-device-bt.c \
	../libnm/nm-device-ethernet.c \
	../libnm/nm-device-generic.c \
	../libnm/nm-device-tun.c \
	../libnm/nm-device-infiniband.c \
	../libnm/nm-device-ip-tunnel.c \
	../libnm/nm-device-macvlan.c \
	../libnm/nm-device-modem.c \
	../libnm/nm-device-olpc-mesh.c \
	../libnm/nm-device-team.c \
	../libnm/nm-device-vlan.c \
	../libnm/nm-device-vxlan.c \
	../libnm/nm-device-wifi.c \
	../libnm/nm-device-wimax.c \
	../libnm/nm-device.c \
	../libnm/nm-manager.c \
	../libnm/nm-object.c \
	../libnm/nm-remote-connection.c \
	../libnm/nm-remote-settings.c \
	../libnm/nm-vpn-plugin-old.c \
	../libnm/nm-vpn-service-plugin.c \
	../data/org.freedesktop.NetworkManager.policy.in.in \
	../shared/nm-utils/nm-shared-utils.c \
	../src/NetworkManagerUtils.c \
	../src/main.c \
	../src/main-utils.c \
	../src/dhcp/nm-dhcp-dhclient.c \
	../src/dhcp/nm-dhcp-dhclient-utils.c \
	../src/dhcp/nm-dhcp-manager.c \
	../src/dns/nm-dns-manager.c \
	../src/devices/adsl/nm-device-adsl.c \
	../src/devices/bluetooth/nm-bluez-device.c \
	../src/devices/bluetooth/nm-device-bt.c \
	../src/devices/nm-device-bond.c \
	../src/devices/nm-device-bridge.c \
	../src/devices/nm-device-ethernet.c \
	../src/devices/nm-device-ethernet-utils.c \
	../src/devices/nm-device-infiniband.c \
	../src/devices/nm-device-ip-tunnel.c \
	../src/devices/nm-device-macvlan.c \
	../src/devices/nm-device-tun.c \
	../src/devices/nm-device-vlan.c \
	../src/devices/nm-device-vxlan.c \
	../src/devices/team/nm-device-team.c \
	../src/devices/wifi/nm-device-olpc-mesh.c \
	../src/devices/wifi/nm-device-wifi.c \
	../src/devices/wifi/nm-wifi-utils.c \
	../src/devices/wwan/nm-modem-broadband.c \
	../src/nm-config.c \
	../src/nm-iface-helper.c \
	../src/nm-logging.c \
	../src/nm-manager.c \
	../src/settings/plugins/ibft/nms-ibft-plugin.c \
	../src/settings/plugins/ifcfg-rh/nms-ifcfg-rh-reader.c

CATALOGS=$(shell LINGUAS="$(USE_LINGUAS)"; for lang in $$LINGUAS; do printf "$$lang.gmo "; done)

.SUFFIXES:
.SUFFIXES: .po .pox .gmo .mo .msg .cat

AM_DEFAULT_VERBOSITY = 1
INTLTOOL_V_MSGFMT = $(INTLTOOL__v_MSGFMT_$(V))
INTLTOOL__v_MSGFMT_= $(INTLTOOL__v_MSGFMT_$(AM_DEFAULT_VERBOSITY))
INTLTOOL__v_MSGFMT_0 = @echo "  MSGFMT" $@;

.po.pox:
	$(MAKE) $(GETTEXT_PACKAGE).pot
	$(MSGMERGE) $* $(GETTEXT_PACKAGE).pot -o $*.pox

.po.mo:
	$(INTLTOOL_V_MSGFMT)$(MSGFMT) -o $@ $<

.po.gmo:
	$(INTLTOOL_V_MSGFMT)file=`echo $* | sed 's,.*/,,'`.gmo \
	  && rm -f $$file && $(GMSGFMT) -o $$file $<

.po.cat:
	sed -f ../intl/po2msg.sed < $< > $*.msg \
	  && rm -f $@ && gencat $@ $*.msg


all: all-yes

all-yes: $(CATALOGS)
all-no:

$(GETTEXT_PACKAGE).pot: $(POTFILES)
	$(GENPOT)

install: install-data
install-data: install-data-yes
install-data-no: all
install-data-yes: all
	linguas="$(USE_LINGUAS)"; \
	for lang in $$linguas; do \
	  dir=$(DESTDIR)$(localedir)/$$lang/LC_MESSAGES; \
	  $(mkdir_p) $$dir; \
	  if test -r $$lang.gmo; then \
	    $(INSTALL_DATA) $$lang.gmo $$dir/$(GETTEXT_PACKAGE).mo; \
	    echo "installing $$lang.gmo as $$dir/$(GETTEXT_PACKAGE).mo"; \
	  else \
	    $(INSTALL_DATA) $(srcdir)/$$lang.gmo $$dir/$(GETTEXT_PACKAGE).mo; \
	    echo "installing $(srcdir)/$$lang.gmo as" \
		 "$$dir/$(GETTEXT_PACKAGE).mo"; \
	  fi; \
	  if test -r $$lang.gmo.m; then \
	    $(INSTALL_DATA) $$lang.gmo.m $$dir/$(GETTEXT_PACKAGE).mo.m; \
	    echo "installing $$lang.gmo.m as $$dir/$(GETTEXT_PACKAGE).mo.m"; \
	  else \
	    if test -r $(srcdir)/$$lang.gmo.m ; then \
	      $(INSTALL_DATA) $(srcdir)/$$lang.gmo.m \
		$$dir/$(GETTEXT_PACKAGE).mo.m; \
	      echo "installing $(srcdir)/$$lang.gmo.m as" \
		   "$$dir/$(GETTEXT_PACKAGE).mo.m"; \
	    else \
	      true; \
	    fi; \
	  fi; \
	done

# Empty stubs to satisfy archaic automake needs
dvi info ctags tags CTAGS TAGS ID:

# Define this as empty until I found a useful application.
install-exec installcheck:

uninstall:
	linguas="$(USE_LINGUAS)"; \
	for lang in $$linguas; do \
	  rm -f $(DESTDIR)$(localedir)/$$lang/LC_MESSAGES/$(GETTEXT_PACKAGE).mo; \
	  rm -f $(DESTDIR)$(localedir)/$$lang/LC_MESSAGES/$(GETTEXT_PACKAGE).mo.m; \
	done

check: all $(GETTEXT_PACKAGE).pot
	rm -f missing notexist
	srcdir=$(srcdir) $(INTLTOOL_UPDATE) -m
	if [ -r missing -o -r notexist ]; then \
	  exit 1; \
	fi

mostlyclean:
	rm -f *.pox $(GETTEXT_PACKAGE).pot *.old.po cat-id-tbl.tmp
	rm -f .intltool-merge-cache

clean: mostlyclean

distclean: clean
	rm -f Makefile Makefile.in POTFILES stamp-it
	rm -f *.mo *.msg *.cat *.cat.m *.gmo

maintainer-clean: distclean
	@echo "This command is intended for maintainers to use;"
	@echo "it deletes files that may require special tools to rebuild."
	rm -f Makefile.in.in

distdir = ../$(PACKAGE)-$(VERSION)/$(subdir)
dist distdir: $(DISTFILES)
	dists="$(DISTFILES)"; \
	extra_dists="$(EXTRA_DISTFILES)"; \
	for file in $$extra_dists; do \
	  test -f $(srcdir)/$$file && dists="$$dists $(srcdir)/$$file"; \
	done; \
	for file in $$dists; do \
	  test -f $$file || file="$(srcdir)/$$file"; \
	  ln $$file $(distdir) 2> /dev/null \
	    || cp -p $$file $(distdir); \
	done

update-po: Makefile
	$(MAKE) $(GETTEXT_PACKAGE).pot
	tmpdir=`pwd`; \
	linguas="$(USE_LINGUAS)"; \
	for lang in $$linguas; do \
	  echo "$$lang:"; \
	  result="`$(MSGMERGE) -o $$tmpdir/$$lang.new.po $$lang`"; \
	  if $$result; then \
	    if cmp $(srcdir)/$$lang.po $$tmpdir/$$lang.new.po >/dev/null 2>&1; then \
	      rm -f $$tmpdir/$$lang.new.po; \
            else \
	      if mv -f $$tmpdir/$$lang.new.po $$lang.po; then \
	        :; \
	      else \
	        echo "msgmerge for $$lang.po failed: cannot move $$tmpdir/$$lang.new.po to $$lang.po" 1>&2; \
	        rm -f $$tmpdir/$$lang.new.po; \
	        exit 1; \
	      fi; \
	    fi; \
	  else \
	    echo "msgmerge for $$lang.gmo failed!"; \
	    rm -f $$tmpdir/$$lang.new.po; \
	  fi; \
	done

Makefile POTFILES: stamp-it
	@if test ! -f $@; then \
	  rm -f stamp-it; \
	  $(MAKE) stamp-it; \
	fi

stamp-it: Makefile.in.in $(top_builddir)/config.status POTFILES.in
	cd $(top_builddir) \
	  && CONFIG_FILES=$(subdir)/Makefile.in CONFIG_HEADERS= CONFIG_LINKS= \
	       $(SHELL) ./config.status

# Tell versions [3.59,3.63) of GNU make not to export all variables.
# Otherwise a system limit (for SysV at least) may be exceeded.
.NOEXPORT:
