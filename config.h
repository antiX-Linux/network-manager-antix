/* config.h.  Generated from config.h.in by configure.  */
/* config.h.in.  Generated from configure.ac by autoheader.  */

/* Define if building universal (internal helper macro) */
/* #undef AC_APPLE_UNIVERSAL_BUILD */

/* Path to ConsoleKit database */
/* #undef CKDB_PATH */

/* Define if dhcpcd supports IPv6 (6.x+) */
/* #undef DHCPCD_SUPPORTS_IPV6 */

/* Define to path of dnsmasq binary */
#define DNSMASQ_PATH "/usr/sbin/dnsmasq"

/* Define to path of unbound dnssec-trigger-script */
#define DNSSEC_TRIGGER_SCRIPT "/usr/lib/dnssec-trigger/dnssec-trigger-script"

/* Define to 1 if translation of program messages to the user's native
   language is requested. */
#define ENABLE_NLS 1

/* Gettext package */
#define GETTEXT_PACKAGE "NetworkManager"

/* Define to 1 if you have the MacOS X function CFLocaleCopyCurrent in the
   CoreFoundation framework. */
/* #undef HAVE_CFLOCALECOPYCURRENT */

/* Define to 1 if you have the MacOS X function CFPreferencesCopyAppValue in
   the CoreFoundation framework. */
/* #undef HAVE_CFPREFERENCESCOPYAPPVALUE */

/* Define if the GNU dcgettext() function is already present or preinstalled.
   */
#define HAVE_DCGETTEXT 1

/* Define to 1 if you have the <dlfcn.h> header file. */
#define HAVE_DLFCN_H 1

/* Define if the GNU gettext() function is already present or preinstalled. */
#define HAVE_GETTEXT 1

/* Define if you have libgnutls */
#define HAVE_GNUTLS 1

/* Define to 1 if you have the <history.h> header file. */
/* #undef HAVE_HISTORY_H */

/* Define if you have the iconv() function and it works. */
/* #undef HAVE_ICONV */

/* Define to 1 if you have the <inttypes.h> header file. */
#define HAVE_INTTYPES_H 1

/* Define if you have libaudit support */
#define HAVE_LIBAUDIT 1

/* Define to 1 if libsystemd is available */
#define HAVE_LIBSYSTEMD 0

/* Define to 1 if you have the <memory.h> header file. */
#define HAVE_MEMORY_H 1

/* Define if nl80211 has critical protocol support */
#define HAVE_NL80211_CRITICAL_PROTOCOL_CMDS 0

/* Define if you have NSS */
/* #undef HAVE_NSS */

/* Define to 1 if you have the <pppd/pppd.h> header file. */
#define HAVE_PPPD_PPPD_H 1

/* Define to 1 if you have the <readline.h> header file. */
/* #undef HAVE_READLINE_H */

/* Define to 1 if you have the <readline/history.h> header file. */
#define HAVE_READLINE_HISTORY_H 1

/* Define to 1 if you have the <readline/readline.h> header file. */
#define HAVE_READLINE_READLINE_H 1

/* Define to 1 if you have the `secure_getenv' function. */
#define HAVE_SECURE_GETENV 1

/* Define if you have SELinux support */
#define HAVE_SELINUX 1

/* Define to 1 if you have the <stdint.h> header file. */
#define HAVE_STDINT_H 1

/* Define to 1 if you have the <stdlib.h> header file. */
#define HAVE_STDLIB_H 1

/* Define to 1 if you have the <strings.h> header file. */
#define HAVE_STRINGS_H 1

/* Define to 1 if you have the <string.h> header file. */
#define HAVE_STRING_H 1

/* Define if systemd support is available */
#define HAVE_SYSTEMD 0

/* Define to 1 if you have the <sys/stat.h> header file. */
#define HAVE_SYS_STAT_H 1

/* Define to 1 if you have the <sys/types.h> header file. */
#define HAVE_SYS_TYPES_H 1

/* Define to 1 if you have the <unistd.h> header file. */
#define HAVE_UNISTD_H 1

/* Define if you have VLAN_FLAG_LOOSE_BINDING */
#define HAVE_VLAN_FLAG_LOOSE_BINDING 1

/* Define if you have Linux Wireless Extensions support */
#define HAVE_WEXT 1

/* Define to 1 if you have the `__secure_getenv' function. */
/* #undef HAVE___SECURE_GETENV */

/* Enable Gentoo hostname persist method */
/* #undef HOSTNAME_PERSIST_GENTOO */

/* Enable Slackware hostname persist method */
/* #undef HOSTNAME_PERSIST_SLACKWARE */

/* Enable SuSE hostname persist method */
/* #undef HOSTNAME_PERSIST_SUSE */

/* Define to path of iptables binary */
#define IPTABLES_PATH "/sbin/iptables"

/* Define to path of the kernel firmware directory */
#define KERNEL_FIRMWARE_DIR "/lib/firmware"

/* Define to the sub-directory where libtool stores uninstalled libraries. */
#define LT_OBJDIR ".libs/"

/* Path to netconfig */
/* #undef NETCONFIG_PATH */

/* The default value of the logging.audit configuration option */
#define NM_CONFIG_DEFAULT_LOGGING_AUDIT "true"

/* Default configuration option for logging.backend */
#define NM_CONFIG_DEFAULT_LOGGING_BACKEND "syslog"

/* The default value of the auth-polkit configuration option */
#define NM_CONFIG_DEFAULT_MAIN_AUTH_POLKIT "true"

/* Default configuration option for main.dhcp setting */
#define NM_CONFIG_DEFAULT_MAIN_DHCP "dhclient"

/* Default configuration option for main.plugins setting */
#define NM_CONFIG_DEFAULT_MAIN_PLUGINS "ifupdown,ibft"

/* Default value for main.rc-manager setting
   (--with-config-dns-rc-manager-default) */
#define NM_CONFIG_DEFAULT_MAIN_RC_MANAGER "resolvconf"

/* Define the distribution version string */
/* #undef NM_DIST_VERSION */

/* git commit id of the original source code version */
#define NM_GIT_SHA ""

/* Define if more asserts are enabled */
#define NM_MORE_ASSERTS 0

/* Define if more debug logging is enabled */
/* #undef NM_MORE_LOGGING */

/* Name of package */
#define PACKAGE "NetworkManager"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "http://bugzilla.gnome.org/enter_bug.cgi?product=NetworkManager"

/* Define to the full name of this package. */
#define PACKAGE_NAME "NetworkManager"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "NetworkManager 1.6.2"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "NetworkManager"

/* Define to the home page for this package. */
#define PACKAGE_URL ""

/* Define to the version of this package. */
#define PACKAGE_VERSION "1.6.2"

/* Define to path of pppd binary */
#define PPPD_PATH "/usr/sbin/pppd"

/* Path to resolvconf */
#define RESOLVCONF_PATH "/sbin/resolvconf"

/* Define to 1 if ConsoleKit is available */
/* #undef SESSION_TRACKING_CONSOLEKIT */

/* Define to 1 if libsystemd-login is available */
/* #undef SESSION_TRACKING_SYSTEMD */

/* The size of `dev_t', as computed by sizeof. */
#define SIZEOF_DEV_T 8

/* Define to 1 if you have the ANSI C header files. */
#define STDC_HEADERS 1

/* Define to 1 to use ConsoleKit2 suspend api */
#define SUSPEND_RESUME_CONSOLEKIT 1

/* Define to 1 to use systemd suspend api */
/* #undef SUSPEND_RESUME_SYSTEMD */

/* Define to 1 to use UPower suspend api */
/* #undef SUSPEND_RESUME_UPOWER */

/* Define to 1 if libsystemd-journald is available */
#define SYSTEMD_JOURNAL 0

/* Define to path to system CA certificates */
#define SYSTEM_CA_PATH "/etc/ssl/certs"

/* Define python path for test binary */
#define TEST_NM_PYTHON "/usr/bin/python"

/* Enable extensions on AIX 3, Interix.  */
#ifndef _ALL_SOURCE
# define _ALL_SOURCE 1
#endif
/* Enable GNU extensions on systems that have them.  */
#ifndef _GNU_SOURCE
# define _GNU_SOURCE 1
#endif
/* Enable threading extensions on Solaris.  */
#ifndef _POSIX_PTHREAD_SEMANTICS
# define _POSIX_PTHREAD_SEMANTICS 1
#endif
/* Enable extensions on HP NonStop.  */
#ifndef _TANDEM_SOURCE
# define _TANDEM_SOURCE 1
#endif
/* Enable general extensions on Solaris.  */
#ifndef __EXTENSIONS__
# define __EXTENSIONS__ 1
#endif


/* Version number of package */
#define VERSION "1.6.2"

/* Define if you have Bluez 5 libraries */
#define WITH_BLUEZ5_DUN 1

/* Define if you want connectivity checking support */
#define WITH_CONCHECK 1

/* Define if you have dhclient */
#define WITH_DHCLIENT 1

/* Define if you have dhcpcd */
#define WITH_DHCPCD 0

/* Define if JANSSON is enabled */
#define WITH_JANSSON 1

/* Define if you have libsoup */
#define WITH_LIBSOUP 1

/* Define if you have ModemManager1 support */
#define WITH_MODEM_MANAGER_1 1

/* Define if you have oFono support (experimental) */
#define WITH_OFONO 0

/* whether to compile polkit support */
#define WITH_POLKIT 1

/* Define if you have polkit agent */
#define WITH_POLKIT_AGENT 1

/* Define if you have PPP support */
#define WITH_PPP 1

/* Whether compilation of ibft setting plugin is enabled */
#define WITH_SETTINGS_PLUGIN_IBFT 1

/* Define if you have Teamd control support */
#define WITH_TEAMDCTL 1

/* Define if you have Wi-Fi support */
#define WITH_WIFI 1

/* Define WORDS_BIGENDIAN to 1 if your processor stores words with the most
   significant byte first (like Motorola and SPARC, unlike Intel). */
#if defined AC_APPLE_UNIVERSAL_BUILD
# if defined __BIG_ENDIAN__
#  define WORDS_BIGENDIAN 1
# endif
#else
# ifndef WORDS_BIGENDIAN
/* #  undef WORDS_BIGENDIAN */
# endif
#endif

/* Define to 1 if on MINIX. */
/* #undef _MINIX */

/* Define to 2 if the system does not provide POSIX.1 features except with
   this defined. */
/* #undef _POSIX_1_SOURCE */

/* Define to 1 if you need to in order for `stat' and other things to work. */
/* #undef _POSIX_SOURCE */

/* Define to `int' if <sys/types.h> does not define. */
/* #undef pid_t */
